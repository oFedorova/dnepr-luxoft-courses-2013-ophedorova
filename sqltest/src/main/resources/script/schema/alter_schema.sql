
ALTER TABLE  product ADD  maker_id INT NOT NULL;
UPDATE product SET product.maker_id =
    (SELECT  makers.maker_id FROM makers WHERE product.maker = makers.maker_name);
ALTER TABLE product DROP maker;
ALTER TABLE  product
	ADD CONSTRAINT fk_product_makers FOREIGN KEY (maker_id) REFERENCES  makers (maker_id);

ALTER TABLE  printer ADD  type_id INT NOT NULL;
UPDATE printer SET printer.type_id =
    (SELECT  printer_type.type_id FROM printer_type WHERE printer.type = printer_type.type_name);
ALTER TABLE printer  DROP type;
ALTER TABLE printer
	ADD CONSTRAINT fk_printer_type FOREIGN KEY (type_id) REFERENCES printer_type (type_id);
ALTER TABLE  printer CHANGE  color  color CHAR( 1 ) CHARACTER SET utf8 NOT NULL DEFAULT  'y';

ALTER TABLE  pc ADD INDEX ind_pc_price(price);
ALTER TABLE  laptop ADD INDEX ind_laptop_price(price);
ALTER TABLE  printer ADD INDEX ind_printer_price(price);
