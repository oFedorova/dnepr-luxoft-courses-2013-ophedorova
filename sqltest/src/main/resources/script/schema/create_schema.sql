CREATE TABLE IF NOT EXISTS product (
	maker VARCHAR(50),
	model VARCHAR(50) NOT NULL,
	type VARCHAR(50) NOT NULL,
	CONSTRAINT pk_product PRIMARY KEY(model)
);

CREATE TABLE IF NOT EXISTS pc (
	id INT,
	model VARCHAR(50) NOT NULL,
	speed SMALLINT,
	ram SMALLINT,
	hd FLOAT,
	cd VARCHAR(10),
	price DOUBLE,
  	CONSTRAINT pk_pc PRIMARY KEY(id),
	CONSTRAINT fk_pc_product FOREIGN KEY(model) REFERENCES product(model)
);

CREATE TABLE IF NOT EXISTS laptop (
	id INT,
	model VARCHAR(50),
	speed SMALLINT,
	ram SMALLINT,
	hd FLOAT,
	screen SMALLINT,
	price DOUBLE,
 	CONSTRAINT pk_laptop PRIMARY KEY(id),
	CONSTRAINT fk_laptop_product FOREIGN KEY(model) REFERENCES product(model)
);

CREATE TABLE IF NOT EXISTS printer (
	id INT,
	model VARCHAR(50) NOT NULL,
	color CHAR(1),
	type VARCHAR(50) NOT NULL,
	price DOUBLE,
	CONSTRAINT pk_printer PRIMARY KEY(id),
	CONSTRAINT fk_printer_product FOREIGN KEY(model) REFERENCES product(model)
);

CREATE TABLE IF NOT EXISTS makers (
  maker_id INT,
  maker_name varchar(50) NOT NULL,
  maker_adress varchar(200),
  CONSTRAINT pk_makers PRIMARY KEY (maker_id)
);

CREATE TABLE IF NOT EXISTS printer_type (
  type_id INT,
  type_name varchar(50) NOT NULL,
  CONSTRAINT pk_printer_type PRIMARY KEY (type_id)
);