SELECT temp.hd
FROM (

SELECT  hd , COUNT(  hd ) AS c
FROM  pc 
GROUP BY  hd
) AS temp
WHERE temp.c >= 2
ORDER BY  temp.hd DESC;