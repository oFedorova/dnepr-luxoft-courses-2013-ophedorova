SELECT DISTINCT makers.maker_name
FROM  pc 
INNER JOIN product 
	ON product.model = pc.model
INNER JOIN makers 
	ON makers.maker_id = product.maker_id
WHERE  speed >= 450;