SELECT makers.maker_id, AVG( screen ) AS avg_size
FROM  laptop 
INNER JOIN product 
	ON laptop.model = product.model
INNER JOIN makers 
	ON makers.maker_id = product.maker_id
GROUP BY makers.maker_id
ORDER BY avg_size;
