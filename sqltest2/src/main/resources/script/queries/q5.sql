SELECT printer.model,  price 
FROM  printer 
INNER JOIN product 
	ON product.model = printer.model
INNER JOIN makers 
	ON makers.maker_id = product.maker_id
WHERE makers.maker_name ="B"

UNION 

SELECT pc.model,  price 
FROM  pc 
INNER JOIN product 
	ON product.model = pc.model
INNER JOIN makers 
	ON makers.maker_id = product.maker_id
WHERE makers.maker_name ="B"

UNION

SELECT laptop.model,  price 
FROM  laptop 
INNER JOIN product 
	ON product.model = laptop.model
INNER JOIN makers 
	ON makers.maker_id = product.maker_id
WHERE makers.maker_name ="B";