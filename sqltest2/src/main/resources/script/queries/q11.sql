SELECT temp.maker_id , temp.type
FROM (
SELECT  type , COUNT(  type ) AS counts,  maker_id 
FROM  product 
GROUP BY  type ,  maker_id
) AS temp
WHERE temp.counts >= 2;