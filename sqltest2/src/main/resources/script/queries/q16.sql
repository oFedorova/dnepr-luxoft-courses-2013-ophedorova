SELECT maker_id, COUNT( maker_id ) AS mod_count
FROM product
WHERE type = 'PC'
GROUP BY maker_id, type 
HAVING mod_count >= 3;
