SELECT product.type, laptop.model, speed
FROM laptop, product 
WHERE laptop.model = product.model 
  AND speed < (SELECT MIN(speed)
  FROM pc);