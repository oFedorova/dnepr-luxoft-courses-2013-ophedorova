SELECT AVG(speed) as avg_speed
FROM pc 
INNER JOIN product
  ON pc.model = product.model  
INNER JOIN makers
  ON makers.maker_id = product.maker_id
WHERE makers.maker_name = "A";