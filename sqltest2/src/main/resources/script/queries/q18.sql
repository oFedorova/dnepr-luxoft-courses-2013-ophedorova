SELECT model
FROM (

SELECT temp.model AS model, temp.price
FROM (

SELECT  model ,  price 
FROM  laptop 
HAVING  price = ( 
SELECT MAX(  price ) 
FROM  laptop ) 
	UNION 
SELECT  model ,  price 
FROM  pc 
HAVING  price = ( 
SELECT MAX(  price ) 
FROM  pc ) 
	UNION 
SELECT  model ,  price 
FROM  printer 
HAVING  price = (SELECT MAX(  price ) 
FROM  printer )
) AS temp
HAVING temp.price = MAX( temp.price )
) AS temp2;