package com.luxoft.dnepr.courses.regular.unit5.dao;

//import  static  org.easymock.classextension.EasyMock.*;


import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Assert;
import org.junit.Test;


/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 10.05.13
 * Time: 22:00
 * To change this template use File | Settings | File Templates.
 */
public class EmployeeDaoImplTest {



    @Test
    public void saveTest() {

        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        Entity entityE = new Employee();
        Employee emploee = new Employee();
        try {
            employeeDao.save((Employee) entityE);
            employeeDao.save(emploee);

        } catch (UserAlreadyExist userAlreadyExist) {
            userAlreadyExist.printStackTrace();
        }
    }

    @Test(expected = ClassCastException.class)
    public void saveTest2() throws UserAlreadyExist, ClassCastException {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        Entity entityR = new Redis();
        Redis redis = new Redis();
        employeeDao.save((Employee) entityR);
//        employeeDao.save(redis);
    }


    @Test
    public void getTest() throws UserAlreadyExist {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        Assert.assertNull(employeeDao.get(new Long(25)));

        employeeDao.save(new Employee());
        employeeDao.save(new Employee());
        Assert.assertNotNull(employeeDao.get(new Long(2)));
    }

    @Test
    public void updateTest() throws UserAlreadyExist, UserNotFound {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        employeeDao.save(new Employee());
        employeeDao.save(new Employee(null, 300));
        Assert.assertFalse( (employeeDao.get(new Long(2))).equals(employeeDao.update(new Employee(new Long(2), 500)) ) );
    }

    @Test (expected = UserNotFound.class)
    public void updateUserNotFoundTest() throws UserAlreadyExist, UserNotFound {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        employeeDao.save(new Employee());
        employeeDao.save(new Employee(null, 300));
        employeeDao.update(new Employee(new Long(20), 500));
    }

    @Test
    public void deleteTest() throws UserAlreadyExist {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        employeeDao.save(new Employee());
        employeeDao.save(new Employee(null, 300));
        Assert.assertTrue(employeeDao.delete(new Long(2)));
        Assert.assertFalse(employeeDao.delete(new Long(2)));
        Assert.assertFalse(employeeDao.delete(new Long(20)));
    }

}
