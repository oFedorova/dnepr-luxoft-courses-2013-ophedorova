package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public class WalletTest {

    @Test(expected = WalletIsBlockedException.class)
    public void checkWithdrawalBlockedTest() throws WalletIsBlockedException {
        Wallet wallet1 = new Wallet();
        Assert.assertTrue(WalletStatus.ACTIVE == wallet1.getStatus());
        wallet1.setStatus(WalletStatus.BLOCKED);
        try {

            wallet1.checkWithdrawal(new BigDecimal(10.99));

        } catch (InsufficientWalletAmountException e) {
            System.out.println(e.getMessage());
        }
    }


    @Test(expected = InsufficientWalletAmountException.class)
    public void checkWithdrawalInsufficientWalletAmountTest() throws InsufficientWalletAmountException {
        Wallet wallet1 = new Wallet();
        try {

            wallet1.checkWithdrawal(new BigDecimal(10.99));

        } catch (WalletIsBlockedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void withdrawTest() {
        Wallet wallet1 = new Wallet();
        BigDecimal amount = (new BigDecimal(10.99)).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal init = wallet1.getAmount();
        wallet1.withdraw(amount);
        Assert.assertTrue( (wallet1.getAmount().add(amount)).abs().compareTo(init.abs()) == 0);
    }



    @Test(expected = WalletIsBlockedException.class)
    public void checkTransferBlockedTest() throws WalletIsBlockedException{
        Wallet wallet1 = new Wallet();
        Assert.assertTrue(WalletStatus.ACTIVE == wallet1.getStatus());
        wallet1.setStatus(WalletStatus.BLOCKED);
        try {

            wallet1.checkTransfer(new BigDecimal(10.99));

        } catch (LimitExceededException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test(expected = LimitExceededException.class)
    public void checkTransferLimitExceededExceptionTest() throws LimitExceededException {
        Wallet wallet1 = new Wallet();
        try {

            wallet1.checkTransfer(new BigDecimal(100000.99));

        } catch (WalletIsBlockedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void transferTest()  {
        Wallet wallet1 = new Wallet();
        BigDecimal amount = (new BigDecimal(1000.99)).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        wallet1.transfer(amount);
        Assert.assertTrue(amount.compareTo(wallet1.getAmount()) == 0);
    }

}
