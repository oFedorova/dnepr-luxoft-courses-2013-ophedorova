package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 29.04.13
 * Time: 2:32
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {

    EqualSet equalSet = new EqualSet();

    @Test
    public void testSize() {
        assertEquals(0, equalSet.size());
        equalSet.add(1);
        equalSet.add(2);
        assertEquals(2, equalSet.size());
    }

    @Test
    public void testIsEmpty() {
        equalSet.clear();
        assertEquals(true, equalSet.isEmpty());
        equalSet.add(1);
        assertEquals(false, equalSet.isEmpty());
    }

    public void testContains() {
        equalSet.clear();
        for (int i = 0; i < 7; ) {
            equalSet.add(new Integer(i));
            i += 2;
        }
        equalSet.add(12);
        assertEquals(true, equalSet.contains(12));

    }

    public void testIterator() throws Exception {

    }


    public void testRemove() throws Exception {

    }

    public void testContainsAll() throws Exception {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 7; ) {
            list.add(new Integer(i));
            i += 2;
        }
        EqualSet collection = new EqualSet(list);
        equalSet.clear();
        equalSet.addAll(list);
        equalSet.add(12);
        assertEquals(true, equalSet.containsAll(list));

    }



    public void testClone() throws CloneNotSupportedException {
        equalSet.clear();
        for (int i = 0; i < 7; ) {
            equalSet.add(new Integer(i));
            i += 2;
        }
        EqualSet cloned = (EqualSet) equalSet.clone();
        assertTrue(equalSet != cloned);
        equalSet.add(25);
        cloned.remove(2);
        assertFalse(equalSet.equals(cloned));

    }
}
