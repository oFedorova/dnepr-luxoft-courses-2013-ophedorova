package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA. User: Phedorova Date: 12.05.13 Time: 7:33 To
 * change this template use File | Settings | File Templates.
 */
public class RedisDaoImplTest {

    //    IDao<Redis> redisDao = new RedisDaoImpl();
    @Test
    public void saveTest() {
        IDao<Redis> redisDao = new RedisDaoImpl();

        Entity entityR = new Redis();
        Redis redis = new Redis();
        try {
            redisDao.save((Redis) entityR);
            redisDao.save(redis);

        } catch (UserAlreadyExist userAlreadyExist) {
            userAlreadyExist.printStackTrace();
        }
    }

    @Test(expected = ClassCastException.class)
    public void saveTest2() throws UserAlreadyExist, ClassCastException {
        IDao<Redis> redisDao = new RedisDaoImpl();
        Entity entityE = new Employee();
        Employee redis = new Employee();
        redisDao.save((Redis) entityE);
//        redisDao.save(redis);
    }

    @Test
    public void getTest() throws UserAlreadyExist {
        IDao<Redis> redisDao = new RedisDaoImpl();
        Assert.assertNull(redisDao.get(new Long(55)));


        redisDao.save(new Redis(new Long(300),1));
        Assert.assertNotNull(redisDao.get(new Long(300)));
    }

    @Test
    public void updateTest() throws UserAlreadyExist, UserNotFound {
        IDao<Redis> redisDao = new RedisDaoImpl();
        Redis entity = new Redis(new Long(50), 300);
        redisDao.save(entity);
        Redis entityUpdate = redisDao.update(new Redis(new Long(50), 500));
        Assert.assertFalse((entity).equals(entityUpdate));
    }

    @Test(expected = UserNotFound.class)
    public void updateUserNotFoundTest() throws UserAlreadyExist, UserNotFound {
        IDao<Redis> redisDao = new RedisDaoImpl();
        redisDao.update(new Redis(new Long(20), 500));
    }

    @Test
    public void deleteTest() throws UserAlreadyExist {
        IDao<Redis> redisDao = new RedisDaoImpl();
        redisDao.save(new Redis(null, 300));
        Assert.assertTrue(redisDao.delete(new Long(1)));
        Assert.assertFalse(redisDao.delete(new Long(1)));
        Assert.assertFalse(redisDao.delete(new Long(20)));
    }
}
