package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;


public class BookTest {

    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();
        cloned.setPublicationDate(new GregorianCalendar(2013, 3, 1).getTime());
        assertFalse(book.equals(cloned));
    }

    @Test
    public void testEqualsClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();
        Book cloned2 = (Book)cloned.clone();
        assertTrue(book != cloned);
        assertTrue(cloned != cloned2);
    }

    @Test
    public void testEqualsTrue() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        assertTrue(book.equals(book));
        Book cloned = (Book)book.clone();
        assertTrue(book.equals(cloned));
        assertTrue(cloned.equals(book));
        Book cloned2 = (Book)cloned.clone();
        assertTrue(book.equals(cloned2));
    }

    @Test
    public void testEqualsFalse() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        assertFalse(book.equals(null));
        Book cloned = (Book)book.clone();
        cloned.setPublicationDate(new GregorianCalendar(2013, 3, 1).getTime());
        assertFalse(book.equals(cloned));
    }
}
