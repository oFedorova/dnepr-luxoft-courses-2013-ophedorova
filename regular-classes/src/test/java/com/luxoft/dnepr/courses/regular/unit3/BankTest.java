package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;

import static org.junit.Assert.fail;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public class BankTest {

    Bank bank = new Bank(System.getProperty("java.runtime.version"));
    Long idFromUser = new Long(1);
    Long idToUser = new Long(2);


    @Before
    public void init() {
        User user1 = new User();
        User user2 = new User("User-2");
        User user3 = new User("User-3");
        User user4 = new User(new Long(4), "User-4",
                new Wallet(new Long(8), new BigDecimal(950.773), WalletStatus.ACTIVE, new BigDecimal(1550.478)));
        User user5 = new User(new Long(12), "User-12", new Wallet());

        java.util.Map<Long, UserInterface> clients = new HashMap<Long, UserInterface>();
        clients.put(user1.getId(), user1);
        clients.put(user2.getId(), user2);
        clients.put(user3.getId(), user3);
        clients.put(user4.getId(), user4);

        bank.setUsers(clients);

        bank.addUser(user5);

        int i = 1;
        for (Long key : bank.getUsers().keySet()) {
            if (2 == i) idFromUser = key;
            if (4 == i) idToUser = key;
            i++;
        }
    }

    @Test(expected = IllegalJavaVersionError.class)
    public void BankConstructorErrorTest() {
        System.out.println("\nBankConstructorErrorTest()\n");
        Bank bank0 = new Bank("бред");
    }

    @Test
    public void BankConstructorTest() {
        System.out.println("\nBankConstructorTest\n");
        Bank bank0 = new Bank();
        Bank bank1 = new Bank(System.getProperty("java.runtime.version"));
    }

    @Test(expected = NoUserFoundException.class)
    public void makeMoneyTransactionToUserNoUserFound() throws NoUserFoundException {

        idToUser += 100;
        System.out.println(idFromUser + "  " + idToUser);
        try {
            bank.makeMoneyTransaction(idFromUser, idToUser, new BigDecimal(105.05));
            fail("NoUserFoundException!!!");
        } catch (TransactionException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test(expected = NoUserFoundException.class)
    public void makeMoneyTransactionFromUserNoUserFound() throws NoUserFoundException {

        idFromUser += 100;
        System.out.println(idFromUser + "  " + idToUser);
        try {
            bank.makeMoneyTransaction(idFromUser, idToUser, new BigDecimal(105.05));
            fail("NoUserFoundException!!!");
        } catch (TransactionException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test(expected = TransactionException.class)
    public void makeMoneyTransactionBlocked() throws TransactionException {

        bank.getUsers().get(idFromUser).getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            bank.makeMoneyTransaction(idFromUser, idToUser, new BigDecimal(105.05));
            fail("TransactionException!!!");
        } catch (NoUserFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test(expected = TransactionException.class)
    public void makeMoneyTransactionBlocked2() throws TransactionException {

        bank.getUsers().get(idToUser).getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            bank.makeMoneyTransaction(idFromUser, idToUser, new BigDecimal(105.05));
            fail("TransactionException!!!");
        } catch (NoUserFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test(expected = TransactionException.class)
    public void makeMoneyTransactionInsufficientFunds() throws TransactionException  {

        bank.getUsers().get(idFromUser).getWallet().setAmount(new BigDecimal(10.99));
        try {
            bank.makeMoneyTransaction(idFromUser, idToUser, new BigDecimal(105.05));
            fail("TransactionException!!!");
        } catch (NoUserFoundException e) {
            System.out.println(e.getMessage());
        }

    }

    @Test(expected = TransactionException.class)
    public void makeMoneyTransactionLimitExceeded() throws TransactionException  {

        bank.getUsers().get(idToUser).getWallet().setMaxAmount(new BigDecimal(10.99));
        try {
            bank.makeMoneyTransaction(idFromUser, idToUser, new BigDecimal(105.05));
            fail("TransactionException!!!");
        } catch (NoUserFoundException e) {
            System.out.println(e.getMessage());
        }

    }

}
