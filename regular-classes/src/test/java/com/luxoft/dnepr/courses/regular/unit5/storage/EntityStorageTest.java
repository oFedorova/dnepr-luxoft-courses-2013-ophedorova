package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA. User: Phedorova Date: 12.05.13 Time: 4:19 To
 * change this template use File | Settings | File Templates.
 */
public class EntityStorageTest {

    EntityStorage storage = EntityStorage.getInstance();

    @Before
    public void init() throws UserAlreadyExist {
        Assert.assertNotNull(storage.add(new Employee()));
        Assert.assertNotNull(storage.add(new Employee(null, 50)));
        Assert.assertNotNull(storage.add(new Redis()));
        Assert.assertNotNull(storage.add(new Redis(null, 40)));
    }

    @Test
    public void addTest() throws UserAlreadyExist {
        Assert.assertNotNull(storage.add(new Employee(new Long(1000), 20)));
        Assert.assertNotNull(storage.add(new Redis(new Long(1001), 30)));
    }

    @Test(expected = UserAlreadyExist.class)
    public void addUserAlreadyExistTest() throws UserAlreadyExist {
        Assert.assertNotNull(storage.add(new Employee(new Long(6), 20)));
        Assert.assertNotNull(storage.add(new Redis(new Long(5), 30)));

        storage.add(new Employee(new Long(6), 20));
        storage.add(new Redis(new Long(5), 30));
    }

    @Test
    public void deleteTest() throws UserAlreadyExist {

        if (null == storage.getValue(new Long(5))) {
            Assert.assertNotNull(storage.add(new Redis(new Long(5), 30)));
        }
        Assert.assertTrue(storage.delete(new Long(5)));
        Assert.assertFalse(storage.delete(new Long(5)));
        Assert.assertFalse(storage.delete(new Long(110)));
    }

    @Test
    public void getValueTest() throws UserAlreadyExist {

        if (null == storage.getValue(new Long(55))) {
            Entity entity = new Employee(new Long(55), 20);
            storage.add(entity);
            Assert.assertTrue((entity).equals(storage.getValue(new Long(55))));
        }
        Assert.assertNull(storage.getValue(new Long(51)));
    }

    @Test
    public void updateTest() throws UserNotFound, UserAlreadyExist {
        Entity employee = new Employee(new Long(1), 200);
        Entity redis = new Redis(new Long(4), 300);

        if (storage.isId(new Long(1)))
            Assert.assertTrue((storage.update(employee)).equals(employee));
        if (storage.isId(new Long(4)))
            Assert.assertTrue((storage.update(redis)).equals(redis));

    }

    @Test(expected = UserNotFound.class)
    public void updateUserNotFoundTest() throws UserNotFound {

        storage.update(new Employee(new Long(200), 200));
        storage.update(new Redis(new Long(500), 300));

    }
}
