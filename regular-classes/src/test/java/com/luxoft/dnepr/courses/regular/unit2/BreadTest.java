package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;


/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public class BreadTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        Bread cloned = (Bread) bread.clone();
        assertFalse(bread == cloned);
        assertFalse(!bread.equals(cloned));
        cloned.setWeight(2);
        cloned.setPrice(11.5);
        assertFalse(bread.equals(cloned));
    }

    @Test
    public void testEqualsClone() throws Exception {
        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        Bread cloned = (Bread) bread.clone();
        Bread cloned2 = (Bread) cloned.clone();
        assertTrue(bread != cloned);
        assertTrue(cloned != cloned2);
    }

    @Test
    public void testEqualsTrue() throws Exception {
        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        assertTrue(bread.equals(bread));
        Bread cloned = (Bread) bread.clone();
        assertTrue(bread.equals(cloned));
        assertTrue(cloned.equals(bread));
        Bread cloned2 = (Bread) cloned.clone();
        assertTrue(bread.equals(cloned2));
    }

    @Test
    public void testEqualsFalse() throws Exception {
        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        assertFalse(bread.equals(null));
        Bread cloned = (Bread) bread.clone();
        cloned.setWeight(0.8);
        assertFalse(bread.equals(cloned));
    }
}
