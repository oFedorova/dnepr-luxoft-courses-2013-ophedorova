package com.luxoft.dnepr.courses.regular.unit18.view;

import com.luxoft.dnepr.courses.regular.unit18.controller.ApplicationController;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 28.06.13
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
public class ApplicationRun {
    private Scanner scanner;
    private PrintStream output;


    public ApplicationRun(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void run() {
        ApplicationController controller = ApplicationController.getController();
        controller.start();
        output.println(info());
        output.print(controller.question());
        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    private boolean execute(String command, ApplicationController controller) {
        switch (command.toLowerCase()) {
            case Command.HELP: {
                return commandHelp();
            }
            case Command.ANSWER_YES:
            case Command.ANSWER_NO: {
                return commandResponse(command, controller);
            }
            case Command.REPLACEMENT_VOCABULARY:{
                return commandReplacement(controller);
            }
            case Command.EXIT:{
                return commandExit(controller);
            }
            case Command.ABOUT:{
                return commandAbout();
            }
            default:{
                return commandInvalid();
            }
        }
    }

    private boolean commandAbout() {
        output.println("Console Lingustic analizator v1 application.\n" +
                "Приложение для статистического лингвистического анализа языкового багажа информации индивидов\n" +
                "Author: (Tushar Brahmacobalol & Phedorova)\n" +
                "(C) Luxoft 2013");
        return true;
    }


    private boolean commandInvalid() {
        output.println(Command.INVALID_COMMAND);
        return true;
    }

    private boolean commandExit(ApplicationController controller) {
        output.println(controller.result());
        return false;
    }

    private boolean commandResponse(String command, ApplicationController controller) {
        controller.collectionProcess(command);
        output.println(controller.question());
        return true;
    }

    private boolean commandReplacement(ApplicationController controller){
        output.println(controller.Replacement());
        output.println(controller.question());
        return true;
    }

    private boolean commandHelp() {
        output.println("Usage: \n" +
                "\thelp  - prints this message\n" +
                "\tabout - Lingustic analizator v1\n"+
                "\ty     - known word\n"+
                "\tn     - unknown word\n"+
                "\tset   - replacement vocabulary\n"+
                "\texit  - exits application\n");
        return true;
    }

    private String info(){
        return "Lingustic analizator v1,\n"
                + "autor Tushar Brahmacobalol for help type help,\n" +
                "answer y/n question, your english knowledge will give you";
    }
}
