package com.luxoft.dnepr.courses.regular.unit2;


import java.util.Date;
//import sun.util.calendar.BaseCalendar;

public class Book extends AbstractProduct implements Product {

    private Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date time) {
        this.publicationDate = publicationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (publicationDate != null ? !publicationDate.equals(book.publicationDate) : book.publicationDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 23 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
        return result;
    }

    @Override
    public Object clone()  {
        Book duplicate = null;
        duplicate = (Book)super.clone();
        duplicate.publicationDate = (Date)publicationDate.clone();  //поддерживающий клонирование
        return duplicate;
    }

    @Override
    public String toString() {
        return "Book{" +  super.toString()+
                " \npublicationDate=" + publicationDate +
                '}';
    }

    /*@Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    } */
}
