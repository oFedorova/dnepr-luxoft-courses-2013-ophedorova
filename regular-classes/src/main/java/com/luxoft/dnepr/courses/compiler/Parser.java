package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 17.04.13
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */
public class Parser {
    // These are the token types.
    final int NONE = 0;
    final int DELIMITER = 1;
    final int NUMBER = 2;

    // These are the types of syntax errors.
    /**
     * Синтаксическая ошибка
     */
    ErrorParserMessages msgerror = ErrorParserMessages.UNKNOWN_ERROR;

    // This token indicates end-of-expression.
    final String EOE = "\0";

    private String exp;   // refers to expression string
    private int expIdx;   // current index into the expression
    private String token; // holds current token
    private int tokType;  // holds token's type

    private Compiler compiler;

    {
        compiler = new Compiler();
    }

    private ByteArrayOutputStream resultByte;

    public Parser(ByteArrayOutputStream resultByte) {
        this.resultByte = resultByte;
    }

    /**
     * Определяет является ли символ действием
     *
     * @param c
     * @return true - если символ, являтся действием  и false - если нет
     */
    private boolean isDelim(char c) {
        if ((" +-/*()".indexOf(c) != -1))
            return true;
        return false;
    }

    /**
     * Возвращает код ошибки
     *
     * @param error название ошибки
     * @throws ParserException
     */
    private void handleErr(ErrorParserMessages error) throws ParserException {
        throw new ParserException(error.toString());
    }

    /**
     * Возвращает очередную лексему.
     */
    private void getToken() {
        tokType = NONE;
        token = "";
        if (expIdx == exp.length()) { //проверить конец выражения
            token = EOE;
            return;
        }
        // Игнорировать пробелы.
        while (expIdx < exp.length() &&
                Character.isWhitespace(exp.charAt(expIdx))) {
            ++expIdx;
        }

        // проверить конец выражения.
        if (expIdx == exp.length()) {
            token = EOE;
            return;
        }

        if (isDelim(exp.charAt(expIdx))) { // разделитель-оператор
            token += exp.charAt(expIdx);
            expIdx++;
            tokType = DELIMITER;
        } else if (Character.isDigit(exp.charAt(expIdx))) { // Число
            while (!isDelim(exp.charAt(expIdx))) {
                token += exp.charAt(expIdx);
                expIdx++;
                if (expIdx >= exp.length()) {
                    break;
                }
            }
            tokType = NUMBER;
        } else { // Неизвестный символ
            token = EOE;
            return;
        }
    }

    /**
     * @param expstr
     * @return
     * @throws ParserException
     */
    public double evaluate(String expstr) /*throws ParserException*/ {
        double result;
        exp = expstr;
        expIdx = 0;

        getToken();
        if (token.equals(EOE)) {
            msgerror = ErrorParserMessages.NO_EXPRESSION;
            //handleErr(msgerror);
        }
        result = evalAddSub();

        if (!token.equals(EOE)) {
            msgerror = ErrorParserMessages.SYNTAX_ERROR;
            //handleErr(msgerror);
        }

        return result;
    }

    /**
     * Сложение или вычитание двух слагаемых.
     */
    private double evalAddSub() /*throws ParserException */ {
        char op;
        double result;
        double partialResult;
        result = evalDivMul();

        while ((op = token.charAt(0)) == '+' || op == '-') {
            getToken();
            partialResult = evalDivMul();
            compiler.addCommand(resultByte, VirtualMachine.PUSH, result);

            switch (op) {
                case '-':
                    compiler.addCommand(resultByte, VirtualMachine.PUSH, partialResult);
                    compiler.addCommand(resultByte, VirtualMachine.SWAP);
                    compiler.addCommand(resultByte, VirtualMachine.SUB);
                    result = result - partialResult;
                    break;

                case '+':
                    compiler.addCommand(resultByte, VirtualMachine.PUSH, partialResult);
                    compiler.addCommand(resultByte, VirtualMachine.ADD);
                    result = result + partialResult;
                    break;
            }
        }
        return result;
    }


    /**
     * Умножение или деление двух множителей.
     */
    private double evalDivMul()/* throws ParserException*/ {
        char op;
        double result;
        double partialResult;

        result = evalUnary();

        while ((op = token.charAt(0)) == '*' ||
                op == '/') {
            getToken();
            partialResult = evalUnary();
            compiler.addCommand(resultByte, VirtualMachine.PUSH, result);

            switch (op) {
                case '*':
                    compiler.addCommand(resultByte, VirtualMachine.PUSH, partialResult);
                    compiler.addCommand(resultByte, VirtualMachine.MUL);

                    result = result * partialResult;
                    break;

                case '/':
                    if (partialResult == 0.0) {
                        msgerror = ErrorParserMessages.DIVISION_BY_ZERO;
                        //handleErr(msgerror);
                    }
                    compiler.addCommand(resultByte, VirtualMachine.PUSH, partialResult);
                    compiler.addCommand(resultByte, VirtualMachine.SWAP);
                    compiler.addCommand(resultByte, VirtualMachine.DIV);

                    result = result / partialResult;
                    break;

            }
        }
        return result;
    }

    /**
     * Умножение унарных операторов + и -.
     */
    private double evalUnary()/* throws ParserException*/ {
        double result;
        String op;

        op = "";
        if ((tokType == DELIMITER) &&
                token.equals("+") || token.equals("-")) {
            op = token;
            getToken();
        }
        result = evalBrsckets();

        if (op.equals("-")) {
            result = -result;
        }

        return result;
    }

    /**
     * Вычисление выражения в скобках.
     */
    private double evalBrsckets()/* throws ParserException*/ {
        double result;

        if (token.equals("(")) {
            getToken();
            result = evalAddSub();
            if (!token.equals(")")) {
                msgerror = ErrorParserMessages.BRACKETS_ERROR;
                // handleErr(msgerror);
            }
            getToken();
        } else {
            result = atom();
        }

        return result;
    }

    /**
     * Получение значения в скобках.
     *
     * @return значение числа или переменной
     * @throws ParserException
     */
    private double atom() /*throws ParserException*/ {
        double result = 0.0;

        switch (tokType) {
            case NUMBER:
                try {
                    result = Double.parseDouble(token);
                } catch (NumberFormatException exc) {
                    msgerror = ErrorParserMessages.SYNTAX_ERROR;
                    //handleErr(msgerror);
                }
                getToken();
                break;
            default:
                msgerror = ErrorParserMessages.SYNTAX_ERROR;
                //handleErr(msgerror); /* иначе синтаксическая ошибка в выражении */
                break;
        }
        return result;
    }


}
