package com.luxoft.dnepr.courses.regular.unit18.model;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 28.06.13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
public class Vocabulary {

    private List<String> vocabulary;
    private final String DEFAULT_VOCABULARY = "sonnets";
    private final static int BLOCK_SIZE = 500;
    private final static int MIN_WORD_SIZE = 3;

    public Vocabulary() {
        setVocabulary(SetDictionary.getDictionary(DEFAULT_VOCABULARY));
    }

    public Vocabulary(File file) {
        setVocabulary(file);
    }

    public void setVocabulary(File file) {
        List<String> list = Arrays.asList(loadVocabulary(file));
        this.vocabulary = new ArrayList<String>(list);

    }

    public String getWord(int index) throws IndexOutOfBoundsException {
        return vocabulary.get(index);
    }

    public String remove(int index) throws IndexOutOfBoundsException {
        return vocabulary.remove(index);
    }

    public int getSizeVocabulary() {
        return vocabulary.size();
    }

    private String[] loadVocabulary(File file) {
        Set<String> dictionary = new ConcurrentSkipListSet<>();
        StringBuilder string = new StringBuilder();
        String line;
        try (LineNumberReader reader = new LineNumberReader(new BufferedReader(new FileReader(file)))) {
            while ((line = reader.readLine()) != null) {
                string.append(line).append(" ");

                while ((line = reader.readLine()) != null && reader.getLineNumber() % BLOCK_SIZE != 0) {
                    string.append(line).append(" ");
                }

                token(string.toString(), dictionary);
                string = new StringBuilder();
            }
        } catch (IOException e) {
            vocabulary = new ArrayList<String>();
            e.printStackTrace();
        }
        return dictionary.toArray(new String[]{});
    }


    private void token(final String string, final Set<String> dictionary) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                StringTokenizer tokenizer = new StringTokenizer(string.toString(), " .,:;?!()");
                while (tokenizer.hasMoreTokens()) {
                    String word = tokenizer.nextToken();
                    if (word.length() > MIN_WORD_SIZE) {
                        dictionary.add(word.toLowerCase());
                    }
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Vocabulary.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
