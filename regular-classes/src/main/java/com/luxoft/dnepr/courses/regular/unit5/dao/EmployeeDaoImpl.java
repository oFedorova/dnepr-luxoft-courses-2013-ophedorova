package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 *
 * В пакете com.luxoft.dnepr.courses.regular.unit5.dao создать интерфейс IDao<Entity> (слайд 40)
 * и его реализации EmployeeDaoImpl и RedisDaoImpl, с использованием Generic-ов
 */
public class EmployeeDaoImpl extends AbstractIDao<Employee> {

    public  EmployeeDaoImpl() {
    }

    public <E extends Employee>EmployeeDaoImpl(E entity) {
        super(entity);
    }
}
