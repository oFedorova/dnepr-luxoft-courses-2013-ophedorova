package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 29.04.13
 * Time: 2:09
 * класс com.luxoft.dnepr.courses.regular.unit4.EqualSet который реализует интерфейс java.util.Set<E>.
 * EqualSet действует как обычное множество, но при сравнении элементов руководствуется только методом equals(Object o).
 * Класс EqualSet имеет два публичных конструктора. Один - без параметров по умолчанию.
 * Второй - принимающий коллекцию, которая будет добавлена к EqualSet. !!! null
 * можно добавлять в EqualSet, но только один раз
 */
public class EqualSet<T> implements Set<T>, java.lang.Cloneable, java.io.Serializable {

    private List<T> container = new ArrayList<T>();
    //private static final int MAX_ARRAY_SIZE = 2147483639;

    public EqualSet() {
    }

    public EqualSet(List<T> container) {
        this.container = Collections.unmodifiableList(container);
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public boolean isEmpty() {
        if (container.isEmpty() || null == container || container.size() <= 0) {
            return true;
        } else {
            return false;
        }
    }

    // true возвращает, если набор содержит указанный элемент
    @Override
    public boolean contains(Object obj) {
        Iterator<T> iterator = container.iterator();
        boolean result = false;
        while (iterator.hasNext() && !result) {
            T temp = iterator.next();
            result = equalsElement(temp, obj);
        }  //while
        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return container.iterator();
    }

    @Override
    public Object[] toArray() {
        return container.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return (T[]) container.toArray();
    }

    @Override
/**
 * Removes the specified element from this set if it is present (optional
 * operation). More formally, removes an element e such that (o==null ?
 * e==null : o.equals(e)), if this set contains such an element. Returns
 * true if this set contained the element (or equivalently, if this set
 * changed as a result of the call). (This set will not contain the element
 * once the call returns.)
 */
    public boolean remove(Object obj) {
        if (contains(obj)) {
            Iterator<T> iterator = container.iterator();
            while (iterator.hasNext()) {
                T temp = iterator.next();
                if (equalsElement(temp, obj)) {
                    iterator.remove();
                    return true;
                }
            } //while
        } //if
        else {
            return false;
        }
        return false;
    }

    @Override
/**
 * проверяет наличие всех элементов коллекции objects в данной коллекции
 */
    public boolean containsAll(Collection<?> objects) {
        if (null == objects)
            return false;
        return container.containsAll(objects);
    }

    @Override
    public boolean addAll(Collection<? extends T> ts) {
        if (null == ts)
            return false;
        /*if (!(objects instanceof T))
            return false;*/
        Iterator<?> iterator = ts.iterator();
        boolean result = false;
        while (iterator.hasNext()) {
            Object temp = iterator.next();
            if (result)
                add(temp);
            else
                result = add(temp);
        }
        return result;
    }

    @Override
/**
 *  удаляет все элементы данной коллекции, кроме элементов коллекции objects
 */
    public boolean retainAll(Collection<?> objects) {
        if (null == objects)
            return false;
        return container.retainAll(objects);
    }

    @Override
/**
 * удаляет элементы указанной коллекции, лежащие в данной коллекции
 */
    public boolean removeAll(Collection<?> objects) {
        if (null == objects)
            return false;
        /*if (!(objects instanceof T))
            return false;*/
        Iterator<?> iterator = objects.iterator();
        boolean result = false;
        while (iterator.hasNext()) {
            Object temp = iterator.next();
            if (result)
                remove(temp);
            else
                result = remove(temp);
        }
        return result;
    }

    @Override
    public void clear() {
        container.clear();
    }


    @Override
    public boolean add(Object o) {
        if (container.isEmpty()) {
            container.add((T) o);
            return true;
        }
        if (!contains(o)) {
            container.add((T) o);
            return true;
        } else {
            return false;
        }
    }


    @Override
    public int hashCode() {
        return container.hashCode();
    }

    @Override
    public String toString() {
        return "EqualSet{"
                + "container=" + container.toString()
                + '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof EqualSet))
            return false;

        EqualSet equalSet = (EqualSet) o;

        if (!container.equals(equalSet.container))
            return false;

        return true;
    }

    private boolean equalsElement(T element, Object obj) {
        if (null == element && null == obj) {
            return true;
        }
        if (null == element && null != obj) {
            return false;
        }
        if (null != element) {
            return element.equals(obj);
        }
        return false;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Object obj = super.clone();
        if (container != null && container instanceof Cloneable) {
            ((EqualSet) obj).container = (List) ((ArrayList) container).clone();
        }
        return obj;
    }
}
