package com.luxoft.dnepr.courses.regular.unit18;

import com.luxoft.dnepr.courses.regular.unit18.view.ApplicationRun;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 29.06.13
 * Time: 22:10
 * To change this template use File | Settings | File Templates.
 */
public class Run {
     public static void main(String[] args) {
         new  ApplicationRun(System.out, System.in).run();
    }
}
