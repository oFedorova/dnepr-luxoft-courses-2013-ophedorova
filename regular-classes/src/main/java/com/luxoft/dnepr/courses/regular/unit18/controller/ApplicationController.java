package com.luxoft.dnepr.courses.regular.unit18.controller;

import com.luxoft.dnepr.courses.regular.unit18.model.SetDictionary;
import com.luxoft.dnepr.courses.regular.unit18.model.Vocabulary;
import com.luxoft.dnepr.courses.regular.unit18.view.Command;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 28.06.13
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
public class ApplicationController {
    private static ApplicationController controller;
    private Vocabulary vocabulary;
    private List<String> knownWords = new ArrayList<String>();
    private List<String> unknownWords = new ArrayList<String>();
    private int currentIndex;

    private ApplicationController() {
    }

    public static ApplicationController getController() {
        if (null == controller) {
            controller = new ApplicationController();
        }
        return controller;
    }



    public void collectionProcess(String command) {
        if (command.equals(Command.ANSWER_YES))
            knownWords.add(vocabulary.remove(currentIndex));
        else
            unknownWords.add(vocabulary.remove(currentIndex));
    }

    public String question() {
        return "\nDo you know translation of this word?:\n" + vocabulary.getWord(random());
    }


    public void start() {
        vocabulary = new Vocabulary();
    }

    public String result() {

        double usedWords = (knownWords.size() + unknownWords.size());
        double vocabularySize = vocabulary.getSizeVocabulary() + usedWords;
        double percentage = ((0 == usedWords) ? 0 : (knownWords.size()) / usedWords);
        int words = (int) (vocabularySize * percentage);
        return  ("Your estimated vocabulary is " + words + " words");
    }


    public int random() {
        currentIndex = (int) (Math.random() * vocabulary.getSizeVocabulary());
        return currentIndex;
    }

    public String Replacement(){
        String result = result();
        clear();
        vocabulary.setVocabulary(SetDictionary.getDictionary("koronatsiya"));
       return  result;
    }

    private void clear() {
        knownWords.clear();
        unknownWords.clear();
    }

}
