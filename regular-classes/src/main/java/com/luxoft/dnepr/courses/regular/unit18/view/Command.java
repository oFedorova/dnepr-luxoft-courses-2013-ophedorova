package com.luxoft.dnepr.courses.regular.unit18.view;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 28.06.13
 * Time: 17:25
 * To change this template use File | Settings | File Templates.
 */
public interface Command {
    String HELP = "help";
    String ABOUT = "about";
    String ANSWER_YES = "y";
    String ANSWER_NO = "n";
    String REPLACEMENT_VOCABULARY = "set";
    String INVALID_COMMAND ="Error while reading answer";
    String EXIT = "exit";
}
