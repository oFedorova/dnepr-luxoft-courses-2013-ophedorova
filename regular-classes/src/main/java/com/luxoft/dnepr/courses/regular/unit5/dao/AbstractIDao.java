package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 06.05.13
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
public class AbstractIDao<E extends Entity> implements IDao {

    private EntityStorage storage = EntityStorage.getInstance();
    private E entity;

    public <E extends Entity>AbstractIDao() {
    }

    public  AbstractIDao(E entity) {
        this.entity = entity;
    }

    /**
     * если в save метод передается объект без id (id == null), ему должен быть присвоен id,
     * который равен max(id) from EntityStorage + 1;
     * <p/>
     * если в save метод передается объект с id, которое уже сохранено в EntityStorage, должно быть выброшено исключение
     *
     * @param entity
     * @return
     */
    @Override
    public Entity save(Entity entity) throws UserAlreadyExist {
        if (null == entity)
            throw new UserAlreadyExist();
        if (null == entity.getId())  //объект без id (id == null)
            entity.setId(storage.getNextFreeId());  // присвоен id,который равен max(id) from EntityStorage + 1;
        if (storage.isId(entity.getId())) //id, которое уже сохранено в EntityStorage
            throw new UserAlreadyExist();

        return storage.add( entity);
    }

    /**
     * если в update передан объект без id либо такого id нет в EntityStoragе,
     * должен быть выброшен com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound
     *
     * @param entity
     * @return
     * @throws UserNotFound
     */
    @Override
    public Entity update(Entity entity) throws UserNotFound {
        if (null == entity || null == entity.getId() || !storage.isId(entity.getId()))
            throw new UserNotFound();

        return storage.update( entity);
    }


    /**
     * если в get передан id, которого в нет в EntityStoragе, возвращаем null;
     *
     * @param id
     * @return
     */
    @Override
    public Entity get(Long id) {
        return storage.getValue(id);
    }

    /**
     * если метод delete успешно удалил сущность, вернуть true.
     * Если сущность с таким id отсутствовала, вернуть false;
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(Long id) {
        return storage.delete(id);
    }
}
