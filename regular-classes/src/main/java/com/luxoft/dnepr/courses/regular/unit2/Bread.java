package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct implements Product {

    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Bread)) return false;
        if (!super.equals(obj)) return false;

        Bread bread = (Bread) obj;

        if (Double.compare(bread.weight, weight) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = weight != +0.0d ? Double.doubleToLongBits(weight) : 0L;
        result = 23 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Bread{" +  super.toString()+
                " \nweight=" + weight +
                '}';
    }

}
