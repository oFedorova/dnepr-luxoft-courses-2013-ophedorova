package com.luxoft.dnepr.courses.regular.unit5.model;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 04.05.13
 * Time: 21:01
 * To change this template use File | Settings | File Templates.
 */
public class Redis extends Entity {

    private int weight;

    public Redis() {
    }

    public Redis(int weight) {
        this.weight = weight;
    }

    public Redis(Long id, int weight) {
        super(id);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Redis))
            return false;

        Redis redis = (Redis) o;

        if (weight != redis.weight  &&  super.getId() != redis.getId())
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return weight;
    }

    public String toString(){
        return "Redis :  id: " + super.getId() + " weight: " + weight + "\n";
    }
}