package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public class Wallet implements WalletInterface {

    private static long counterWallet = 1;
    private Long idWallet;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;
    private UserInterface user;

    public Wallet() {
        this.idWallet = new Long(counterWallet++);
        this.amount = new BigDecimal(0.00);
        this.status = WalletStatus.ACTIVE;
        this.maxAmount = new BigDecimal(1000.00);
    }

    public Wallet(Long idWallet, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        counterWallet++;
        this.idWallet = idWallet;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }


    @Override
    public Long getIdWallet() {
        return idWallet;
    }

    @Override
    public void setIdWallet(Long id) {
        if (null != id)
            idWallet = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

    @Override
    public void setAmount(BigDecimal amount) {
        if (null != amount)
            this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        if (null != status)
            this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        if (null != maxAmount)
            this.maxAmount = maxAmount;
    }

    /**
     * @param amountToWithdraw
     * @throws WalletIsBlockedException кошелек заблокирован
     * @throws InsufficientWalletAmountException
     *                                  если сумма снятия(amountToWithdraw) больше, чем сумма(amount) в кошельке
     */
    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException,
            InsufficientWalletAmountException {
        if (null != amountToWithdraw) {
            String message = new String();
            if (WalletStatus.BLOCKED == status) {
                if (null == user)
                    message = "Wallet is blocked";
                else
                    message = "User '" + user.getName() + "' wallet is blocked";

                throw new WalletIsBlockedException(idWallet, message);
            }

            if (amount.compareTo(amountToWithdraw) < 0){
                if (null == user)
                    message = "Has insufficient funds " + this.getAmount() + " < "
                            + amountToWithdraw.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                else
                    message = "User '" + user.getName() + "' has insufficient funds " + this.getAmount() + " < "
                            + amountToWithdraw.setScale(2, BigDecimal.ROUND_HALF_EVEN);

                throw new InsufficientWalletAmountException(idWallet, amountToWithdraw, amount, message );
            }//if
        } // if (null != amountToWithdraw)
    } //checkWithdrawal

    /**
     * списывает amountToWithdraw из кошелька
     *
     * @param amountToWithdraw
     */
    @Override
    public void withdraw(BigDecimal amountToWithdraw){

        amount = amount.subtract(amountToWithdraw);
    }

    /**
     * @param amountToTransfer
     * @throws WalletIsBlockedException кошелек заблокирован
     * @throws LimitExceededException   сумма перевода(amountToTransfer) + сумма(amount) в кошельке превышают
     *                                  лимит кошелька (maxAmount)
     */
    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException,
            LimitExceededException {

        if (null != amountToTransfer) {
            String message = new String();
            if (WalletStatus.BLOCKED == status) {
                if (null == user)
                    message = "Wallet is blocked";
                else
                    message = "User '" + user.getName() + "' wallet is blocked";

                throw new WalletIsBlockedException(idWallet, message);
            }

            if ((amount.add(amountToTransfer)).compareTo(maxAmount) > 0) {
                if (null == user)
                    message = "Wallet limit exceeded " + this.getAmount() + " + "
                            + amountToTransfer.setScale(2, BigDecimal.ROUND_HALF_EVEN) + " > " + this.getMaxAmount();
                else
                    message = "User '" + user.getName() + "' wallet limit exceeded " + this.getAmount() + " + "
                            + amountToTransfer.setScale(2, BigDecimal.ROUND_HALF_EVEN) + " > " + this.getMaxAmount();
                throw new LimitExceededException(idWallet, amountToTransfer, amount, message );
            }  // if
        }  //if (null != amountToTransfer)
    } //checkTransfer


    /**
     * добавляет amountToTransfer к сумме(amount) в кошельке
     *
     * @param amountToTransfer
     */
    @Override
    public void transfer(BigDecimal amountToTransfer) /*throws TransactionException */{

        amount = amount.add(amountToTransfer);
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public String toString() {
        return "IdWallet = " + idWallet + " amount : " + this.getAmount() +
                " status : " + status + " maxAmount : " + this.getMaxAmount();
    }

}
