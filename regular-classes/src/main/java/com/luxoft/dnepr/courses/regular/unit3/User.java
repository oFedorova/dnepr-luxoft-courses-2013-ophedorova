package com.luxoft.dnepr.courses.regular.unit3;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public class User implements UserInterface {

    private static long counterUser =1;
    private Long idUser;
    private String name;
    private WalletInterface wallet;

    public User() {
        this.idUser = new Long(counterUser++);
        this.name = "NewUser";
        this.wallet = new Wallet();
        ((Wallet) this.wallet).setUser(this);
    }

    public User(String name) {
        this.idUser = new Long(counterUser++);
        this.name = name;
        this.wallet = new Wallet();
        ((Wallet) this.wallet).setUser(this);
    }

    public User(Long idUser, String name, WalletInterface wallet) {
        this.idUser = idUser;
        this.name = name;
        this.wallet = wallet;
        ((Wallet) this.wallet).setUser(this);
    }

    @Override
    public Long getId() {
        return idUser;
    }

    @Override
    public void setId(Long id) {
        if (null != id) {
            idUser = id;
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (null != name)
            this.name = name;
    }

    @Override
    public WalletInterface getWallet() {
        return wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        if (null != wallet)
            this.wallet = wallet;
    }

    @Override
    public String toString() {
        return "\n" +this.getClass().getName()+":\nId = " + idUser + " Name : " + name + "\n" + wallet.toString();
    }

}