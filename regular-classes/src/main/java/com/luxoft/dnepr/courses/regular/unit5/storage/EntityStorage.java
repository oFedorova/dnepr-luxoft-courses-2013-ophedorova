package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 04.05.13
 * Time: 21:44
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {

    private static final Map<Long, Entity> entities = new HashMap<Long, Entity>();
    private static EntityStorage ourInstance = null;

    private EntityStorage() {
    }

    public static EntityStorage getInstance() {
        if (null == ourInstance) {
            ourInstance = new EntityStorage();
        }
        return ourInstance;
    }

    public Map<Long, Entity> getEntities() {
        return entities;
    }

    /**
     * Проверяет есть ли объект с заданным ID
     *
     * @param id
     * @return true - если объект с таким ID есть,
     *         false - если нет объекта с таким ID
     */
    public boolean isId(Long id) {
        if (null == id)
            return false;

        return entities.containsKey(id);
    }

    /**
     * Возвращает id, который равен max(id) from EntityStorage + 1
     *
     * @return
     */
    public Long getNextFreeId() {
        if (entities.isEmpty())
            return new Long(1);

        Long maxId = new Long(-1);
        for (Long key : entities.keySet()) {
            if (key.compareTo(maxId) > 0) maxId = key;
        }
        return (++maxId);
    }

    /**
     * @param obj
     * @return
     * @throws UserAlreadyExist
     */
    public Entity add(Entity obj) throws UserAlreadyExist {
        // System.out.println("add  "+"!isId("+id+") ="+!isId(id)+ " || entities.isEmpty() ="+ entities.isEmpty()+" "+obj);

        if (null != obj && (!isId(obj.getId()) || entities.isEmpty())) {
            if(null == obj.getId())
                obj.setId(getNextFreeId());
            entities.put(obj.getId(), obj);
            return obj;

        } else  // id, которое уже сохранено
            throw new UserAlreadyExist();
    }

    /**
     * @param obj
     * @return
     * @throws UserNotFound
     */
    public Entity update(Entity obj) throws UserNotFound {
        if (null != obj && null != obj.getId() && isId(obj.getId()) && !entities.isEmpty()) {
            entities.put(obj.getId(), obj);
            return obj;
        } else  //без id либо такого id нет в EntityStoragе
            throw new UserNotFound();
    }

    /**
     * если передан id, которого в нет в EntityStoragе, возвращаем null;
     *
     * @param id
     * @return
     */
    public Entity getValue(Long id) {
        return entities.get(id);
    }

    /**
     * если метод delete успешно удалил сущность, вернуть true. Если сущность с таким id отсутствовала, вернуть false;
     *
     * @param id
     * @return
     */
    public boolean delete(Long id) {
        if (!isId(id))
            return false;  //сущность с таким id отсутствовала, вернуть false

        if (null == entities.get(id))   //null, если не было никакого значения для key(например удалено).
            // (Возврат null может также указать, что в карте был  null ранее связанный  с key.)
            return false;
        else {
            entities.remove(id);
            return true;
        }
    }

    public String toSting() {
        return "  EntityStorage :" + entities.toString();
    }

}
