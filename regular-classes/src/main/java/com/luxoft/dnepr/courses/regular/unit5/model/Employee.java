package com.luxoft.dnepr.courses.regular.unit5.model;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 04.05.13
 * Time: 21:00
 * To change this template use File | Settings | File Templates.
 */
public class Employee extends Entity {

    private int salary;

    public Employee() {
    }

    public Employee(int salary) {
        this.salary = salary;
    }

    public Employee(Long id, int salary) {
        super(id);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Employee))
            return false;

        Employee employee = (Employee) o;

        if (salary != employee.salary &&
                super.getId() != employee.getId())
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return salary;
    }

    public String toString() {
        return "Employee :  id: " + super.getId() + " salary: " + salary + "\n";
    }
}
