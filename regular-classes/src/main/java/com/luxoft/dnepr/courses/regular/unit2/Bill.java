package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    HashMap<Product,CompositeProduct> mapProduct = new HashMap<Product, CompositeProduct>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     * @param product new product
     */
    public void append(Product product) {
        CompositeProduct pr =  mapProduct.get(product);
        if (pr==null) {
            pr = new CompositeProduct();
            pr.add(product);
        }
        else{
             pr.add(product);
        }
        mapProduct.put(product, pr);
   }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     * @return
     */
    public double summarize() {
        double summ = 0;
        List<Product> positions = getProducts();
        for( Product product: positions){
            summ += product.getPrice();
        }
        return summ;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     * @return
     */
    public List<Product> getProducts() {
        List<Product> product = new ArrayList<Product>();
        for( Product productKey: mapProduct.keySet()){
            product.add(mapProduct.get(productKey));
        }
        Collections.sort(product, new SortedByPrice<Product>());
        return product;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize() ;
    }

    public class SortedByPrice<Product> implements Comparator<Product>{
        @Override
        public int compare(Object obj1, Object obj2) {
            double price1 = ((CompositeProduct) obj1).getPrice();
            double price2 = ((CompositeProduct) obj2).getPrice();

            if(price1 < price2) {
                return 1;
            }
            else if(price1 > price2) {
                return -1;
            }
            else {
                return 0;
            }
        }
    }

}
