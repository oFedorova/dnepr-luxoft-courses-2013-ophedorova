package com.luxoft.dnepr.courses.regular.unit2;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 21.04.13
 * Time: 20:29
 *
 */
public class AbstractProduct implements Product, Cloneable {

    private  String code;
    private String name;
    private double price;

    public AbstractProduct(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    public void setCode(String code) {
        if (null != code) {
           this.code = code;
        }
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setName(String name) {
        if (null != name)
            this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof AbstractProduct)) return false;

        AbstractProduct that = (AbstractProduct) obj;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 23 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String toString() {
        return "{" +
                "\ncode='" + code + '\'' +
                ", \nname='" + name + '\'' +
                ", \nprice=" + price +
                ',';
    }
}
