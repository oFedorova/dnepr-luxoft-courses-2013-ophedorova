package com.luxoft.dnepr.courses.regular.unit7_1;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public final class ThreadProducer {

    private ThreadProducer() {
    }

    /*
    Thread state for a thread which has not yet started
    */
    public static Thread getNewThread() {
        Thread thread = new Thread() {
            public void run() {

            }
        };
        return thread;
    }

    /*Thread state for a runnable thread. A thread in the runnable state is executing in the Java virtual machine
    but it may be waiting for other resources from the operating system such as processor*/
    public static Thread getRunnableThread() {
        Thread thread = new Thread() {
            public void run() {
                while (true) {

                }
            }
        };
        thread.start();
        return thread;
    }

    /*
    Thread state for a thread blocked waiting for a monitor lock. A thread in the blocked state
    is waiting for a monitor lock to enter a synchronized block/method or reenter a
    synchronized block/method after calling Object.wait
    */
    public static Thread getBlockedThread() {
        final Object syn = new Object();
        Thread thread = new Thread() {
            public void run() {
                synchronized (syn) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();

        Thread threadBlocked = new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (syn) {
                    while (true) {

                    }
                }
//
            }
        };
        threadBlocked.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return threadBlocked;
    }

    /*
    * Thread state for a waiting thread. A thread is in the waiting state due to calling one of the following methods:
        •Object.wait    with no timeout
        •Thread.join    with no timeout
        •LockSupport.park
    A thread in the waiting state is waiting for another thread to perform a particular action. For example,
    a thread that has called Object.wait() on an object is waiting for another thread to call Object.notify()
    or Object.notifyAll() on that object. A thread that has called Thread.join() is waiting for a specified thread to
    terminate.
    */
    public static Thread getWaitingThread() {

        return Thread.currentThread();
    }


    public static Thread getTerminatedThread() {

        Thread thread = new Thread() {
            public void run() {
            }
        };
        thread.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return thread;
    }

    /*Thread state for a waiting thread with a specified waiting time. A thread is in the timed waiting state due
     to calling one of the following methods with a specified positive waiting time:
        •Thread.sleep
        •Object.wait    with timeout
        •Thread.join    with timeout
        •LockSupport.parkNanos
        •LockSupport.parkUntil
        */
    public static Thread getTimedWaitingThread() {
         /*Средство синхронизации, которое позволяет ряду потоков ожидать друг друга, чтобы достигнуть
        общей точки барьера. CyclicBarriers полезны в программах, использующих фиксированное число потоков,
        которые должны иногда ожидать друг друга. Барьер вызывают циклически, потому что он может быть снова
        использован после того, как потоки ожидания выпускаются. */
        final Random rnd = new Random();
        Thread thread = new Thread(new Runnable() {
            public void run() {
                for (; ; ) {
                    synchronized (rnd) {
                        rnd.nextDouble();
                        rnd.notify(); //holder.nofifyAll();}
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {
                            return;
                        }
                    }
                }
            }
        });


        thread.setDaemon(true);
        thread.start();

        synchronized (rnd) {
            try {
                while (rnd.nextDouble() <= 0.9) {
                    rnd.wait(100);
                }
            } catch (InterruptedException ex) {
            }
        }

        return thread;
        /*final CyclicBarrier cyclicBarrier = new CyclicBarrier(2, new Runnable() {
            @Override
            public void run() {
            }
        });
        Thread thread0 = new Thread() {
            public void run() {
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException | BrokenBarrierException  e) {
                    e.printStackTrace();
                }
            }
        };
        thread0.start();

        Thread thread = new Thread() {
            public void run() {
                try {
                    cyclicBarrier.await(10L, TimeUnit.SECONDS);
                } catch (InterruptedException | BrokenBarrierException | TimeoutException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread; */
    }


}
