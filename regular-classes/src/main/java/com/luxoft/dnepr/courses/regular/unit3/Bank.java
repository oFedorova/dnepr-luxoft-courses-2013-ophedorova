package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public class Bank implements BankInterface {

    private Map<Long, UserInterface> users;
    private String actualJavaVersion = System.getProperty("java.runtime.version");

    public Bank() {
        this.users = new HashMap<Long, UserInterface>();
    }

    public Bank(String expectedJavaVersion) throws IllegalJavaVersionError {
        if (!this.actualJavaVersion.equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(expectedJavaVersion, this.actualJavaVersion,
                    "Expected : " + expectedJavaVersion + "  -  Actual : " + this.actualJavaVersion);
        }
        this.users = new HashMap<Long, UserInterface>();
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        if (null == this.users)
            this.users = new HashMap<Long, UserInterface>();
        if (null != users)
            this.users.putAll(users);
    }


    public UserInterface addUser(UserInterface user) {
        return users.put(user.getId(), user);
    }

    /**
     * Этот метод переводит сумму (amount) с кошелька пользователя с id=fromUserId на кошелек пользователя с id=toUserId
     *
     * @param fromUserId
     * @param toUserId
     * @param amount
     * @throws NoUserFoundException
     * @throws TransactionException
     */
    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException,
            TransactionException {

        //получить пользователя  с id=fromUserId
        UserInterface fromUser;
        UserInterface toUser;

        fromUser = users.get(fromUserId);
        if (fromUser == null)
            throw new NoUserFoundException(fromUserId, "No User ID='" + fromUserId + "' found");
        toUser = users.get(toUserId);
        if ( null == toUser)
            throw new NoUserFoundException(toUserId, "No User ID='" + toUserId + "' found");

        try {
            fromUser.getWallet().checkWithdrawal(amount);
            toUser.getWallet().checkTransfer(amount);

            fromUser.getWallet().withdraw(amount);
            toUser.getWallet().transfer(amount);

        /*} catch (WalletIsBlockedException|InsufficientWalletAmountException|LimitExceededException e) {
            throw new TransactionException(e.getMessage()); */

        } catch (WalletIsBlockedException e) {
            throw new TransactionException(e.getMessage());
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException(e.getMessage());
        } catch (LimitExceededException e) {
            throw new TransactionException(e.getMessage());
        }

    }


}