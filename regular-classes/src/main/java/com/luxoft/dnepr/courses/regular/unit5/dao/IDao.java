package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * В пакете com.luxoft.dnepr.courses.regular.unit5.dao создать интерфейс IDao<Entity> (слайд 40)
 * и его реализации EmployeeDaoImpl и RedisDaoImpl, с использованием Generic-ов
 */
public interface IDao<E extends Entity> {

    E save(E e) throws UserAlreadyExist;

    E update(E e)throws UserNotFound;

    E get(Long id);

    boolean delete(Long id);

}
