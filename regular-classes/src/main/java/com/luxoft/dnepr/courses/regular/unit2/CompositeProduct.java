package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();
    private final double DISCOUNT_FOR_TWO_POSITIONS = 0.95;
    private final double DISCOUNT_FOR_THREE_OR_MORE = 0.90;

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        if (childProducts.isEmpty())
            return null;
        else
            return childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        if (childProducts.isEmpty())
            return null;
        else
            return childProducts.get(0).getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        if (childProducts.isEmpty())
            return 0.0;
        if (childProducts.size() == 1)
            return childProducts.get(0).getPrice();
        else if (childProducts.size() == 2)
            return discount(DISCOUNT_FOR_TWO_POSITIONS);
        else
            return discount(DISCOUNT_FOR_THREE_OR_MORE);
    }

    private double discount(double percent) {
        double summ = 0;
        for (int i = 0; i < childProducts.size(); i++) {
            //((AbstractProduct) childProducts.get(i)).setPrice(childProducts.get(i).getPrice() * percent);
            summ += childProducts.get(i).getPrice();
        }
        return summ * percent;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
