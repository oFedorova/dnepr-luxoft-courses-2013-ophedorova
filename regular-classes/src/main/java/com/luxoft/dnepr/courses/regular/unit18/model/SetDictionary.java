package com.luxoft.dnepr.courses.regular.unit18.model;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 30.06.13
 * Time: 20:16
 * To change this template use File | Settings | File Templates.
 */
public class SetDictionary {

    private static String PATH_TO_RESOURCES = "regular-classes"+File.separator+"src"+File.separator+"main"+File.separator+"resources";
    private static Map<String, File> dictionary = new HashMap<String, File>();
    static {
        dictionary.put("sonnets",new File(PATH_TO_RESOURCES,"sonnets.txt"));
        dictionary.put("koronatsiya", new File(PATH_TO_RESOURCES, "koronatsiya.txt"));
    }


    public static File getDictionary(String nameDictionary){
        if (dictionary.containsKey(nameDictionary))
            return  dictionary.get(nameDictionary);
        else
            return  null;
    }

    public static void add(String name, File file){
        dictionary.put(name, file);
    }

    public static Set<String> nameDictionary(){
        return dictionary.keySet();
    }
}

