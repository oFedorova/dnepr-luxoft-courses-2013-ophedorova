package com.luxoft.dnepr.courses.compiler;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 17.04.13
 * Time: 23:39
 * To change this template use File | Settings | File Templates.
 */
public enum ErrorParserMessages {
    /**
     * Синтаксическая ошибка
     */
    SYNTAX_ERROR("Синтаксическая ошибка"),

    /**
     * Несбалансированные скобки
     */
    BRACKETS_ERROR("Несбалансированные скобки"),

    /**
     * Нет выражения - пустая строка
     */
    NO_EXPRESSION("Нет выражения - пустая строка"),

    /**
     * Деление на нуль
     */
    DIVISION_BY_ZERO("Деление на нуль"),

    /**
     * Неизвестная ошибка
     */
    UNKNOWN_ERROR("Неизвестная ошибка");

    private String message;

    /**
     * Конструктор enum ErrorMessages
     *
     * @param msg сообщение о ошибке
     */
    ErrorParserMessages(String msg) {
        message = msg;
    }


    @Override
    public String toString() {
        return message;
    }
}
