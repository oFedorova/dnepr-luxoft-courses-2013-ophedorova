package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct implements Product {

    private boolean nonAlcoholic;

    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        this.nonAlcoholic = nonAlcoholic;
    }

    public boolean isNonAlcoholic() {

        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean nonAlcoholic) {
        this.nonAlcoholic = nonAlcoholic;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Beverage))
            return false;
        if (!super.equals(obj))
            return false;

        Beverage beverage = (Beverage) obj;

        if (nonAlcoholic != beverage.nonAlcoholic)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 23 * result + (nonAlcoholic ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Beverage{" +  super.toString()+
                " \nnonAlcoholic=" + nonAlcoholic +
                '}';
    }
    /*@Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    } */
}
