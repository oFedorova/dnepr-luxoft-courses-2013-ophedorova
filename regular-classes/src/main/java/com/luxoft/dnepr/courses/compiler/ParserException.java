package com.luxoft.dnepr.courses.compiler;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 17.04.13
 * Time: 23:44
 * To change this template use File | Settings | File Templates.
 */
public class ParserException extends Exception{
    /**
     * описание ошибки
     */
    String errStr;

    /**
     *Конструктор класса ParserException
     * @param str описание ошибки
     */
    public ParserException(String str) {
        errStr = str;
    }

    @Override
    public String toString() {
        return errStr;
    }
}
