/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luxoft.dnepr.courses.luxoftweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;

//import org.json.simple.JSONObject;
/**
 *
 * @author Phedorova
 */
public class UserServlet extends HttpServlet {
    
    private String users;
    
     @Override
    public void init(){
        ServletContext context = getServletContext();        
        this.users = context.getInitParameter("users");
    }
    

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);        
        String login = request.getParameter("login");
        String password = request.getParameter("password1");
//        RequestDispatcher d = request.getRequestDispatcher("/WEB-INF/javascript/validate.js");
//        d.include(request, response);
        
        Map<String, String> json;
        try {
            json = parserJson(users);
            if (null != json) {

                if (isParam(login, password, json) ) {
                    PrintWriter out = response.getWriter();
                    try {
                       if (null == session) request.getSession();
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<title>Servlet UserServlet</title>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<p> <a href=\"/luxoft-web/logout\">logout</a><br></p>");
                        out.println("<h1>Hello " + login + "!</h1>");
                        out.println("</body>");
                        out.println("</html>");
                    } finally {
                        out.close();
                    }
                } else {
                    response.sendRedirect("/luxoft-web/logout");
                }
            } else {
                Cookie cookie = new Cookie("errorMessage", "invalidate");
                response.addCookie(cookie);
                response.sendRedirect("/myFirstWebApp/");
            }
        } catch (org.json.simple.parser.ParseException ex) {
            response.sendRedirect("/myFirstWebApp/");
        }
    }   
   

    
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }//

    private Map<String, String> parserJson(String jsonText) throws org.json.simple.parser.ParseException {
        JSONParser parser = new JSONParser();
        ContainerFactory containerFactory = new ContainerFactory() {
            public List creatArrayContainer() {
                return new LinkedList();
            }

            public Map createObjectContainer() {
                return new LinkedHashMap();
            }
        };

        return (Map) parser.parse(jsonText, containerFactory);
    }

    private boolean isParam(String login, String password, Map<String, String> json) {
        Iterator iter = json.entrySet().iterator();

        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            if (entry.getKey().equals(login) && entry.getValue().equals(password)) {
                return true;
            }
        }
        return false;
    }
}
