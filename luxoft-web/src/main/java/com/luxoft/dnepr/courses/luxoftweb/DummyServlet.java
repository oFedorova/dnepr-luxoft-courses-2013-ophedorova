/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.luxoft.dnepr.courses.luxoftweb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Phedorova
 */
public class DummyServlet extends HttpServlet {

    private static ConcurrentHashMap<String, Integer> nameAge =
            new ConcurrentHashMap<String, Integer>();

    /**
     * Processes requests for both HTTP
     * <code>GET</code> method
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DummyServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DummyServlet at " + request.getContextPath() + "</h1>");
            out.println("<p>" + request.getProtocol() + " " + response.getStatus() + " " + "</p>");
            out.println("Content-Type: " + response.getContentType() + "</p>");
            out.println("Content-Length: " + request.getContentLength() + "</p>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Processes requests for both HTTP
     * <code>POST</code> and
     * <code>PUT</code> methods
     *
     * @param request servlet request
     * @param response servlet response
     * @param json string
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest500(HttpServletRequest request,
            HttpServletResponse response, String json)
            throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        response.setContentType("application/json; charset=UTF-8");

        PrintWriter out = response.getWriter();
        try {
            out.println(request.getProtocol() + " " + response.getStatus() + " Internal Server Error");
            out.println("Content-Type: " + response.getContentType());
            out.println("Content-Length: " + json.length());
            out.println(json);
        } finally {
            out.close();
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        Integer intAge = Integer.parseInt(age); // throws NumberFormatException
        String json = new String();

        if (!isParameters(name, age)) {
//            HTTP/1.1 500 Internal Server Error
//            Content-Type: application/json; charset=utf-8
//            Content-Length: (total length)
//            {"error": "Illegal parameters"}
            json += "{\"error\": \"Illegal parameters\"}";
            processRequest500(request, response, json);
        } else if (isName(name)) {
//          имя есть  затираем возраст
//            HTTP/1.1 202 Accepted
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            response.setContentType("application/json; charset=UTF-8");
            synchronized (nameAge) {
                nameAge.put(name, intAge);
            }
            PrintWriter out = response.getWriter();
            try {
                out.println(request.getProtocol() + " " + response.getStatus() + " Accepted");
            } finally {
                out.close();
            }
        } else {//нет такого имени
//            HTTP/1.1 500 Internal Server Error
//            Content-Type: application/json; charset=utf-8
//            Content-Length: (total length)
//            {"error": "Name ${name} does not exist"}
            json += "{\"error\": \"Name " + name + " does not exist\"}";
            processRequest500(request, response, json);
        }
    }

    /**
     * Handles the HTTP
     * <code>PUT</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        Map<String, String[]> parameterMap = request.getParameterMap();
//        String name = parameterMap.get("name")[parameterMap.get("name").length - 1];
//        String age = parameterMap.get("age")[parameterMap.get("age").length - 1];

        String name = request.getParameter("name");
        String age = request.getParameter("age");
        Integer intAge = Integer.parseInt(age); // throws NumberFormatException
        String json = new String();

        if (!isParameters(name, age)) {
//            HTTP/1.1 500 Internal Server Error
//            Content-Type: application/json; charset=utf-8
//            Content-Length: (total length)
//            {"error": "Illegal parameters"}
            json += "{\"error\": \"Illegal parameters\"}";
            processRequest500(request, response, json);
        } else if (isName(name)) {
//            HTTP/1.1 500 Internal Server Error
//            Content-Type: application/json; charset=utf-8
//            Content-Length: (total length)
//            {"error": "Name ${name} already exists"}
            json += "{\"error\": \"Name " + name + " already exists\"}";
            processRequest500(request, response, json);
        } else {//нет такого имени
//            HTTP/1.1 201 Created 
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.setContentType("application/json; charset=UTF-8");
            synchronized (nameAge) {
                nameAge.put(name, intAge);
            }

            PrintWriter out = response.getWriter();
            try {
                out.println(request.getProtocol() + " " + response.getStatus() + " Created");
            } finally {
                out.close();
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean isParameters(String name, String age) {

        return (name != null && age != null);
    }

    private boolean isName(String name) {
        for (String entry : nameAge.keySet()) {
            if (name.equals(entry)) {
                return true;
            }
        }
        return false;
    }
}
