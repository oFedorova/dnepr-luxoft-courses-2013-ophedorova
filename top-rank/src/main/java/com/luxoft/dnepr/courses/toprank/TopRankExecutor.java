package com.luxoft.dnepr.courses.toprank;

//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public class TopRankExecutor {

    private double dampingFactor;
    private int numberOfLoopsInRankComputing;

    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }

    public TopRankResults execute(Map<String, String> urlContent) {
        TopRankResults results = new TopRankResults();
        results.parsUrlContent(urlContent);
        Map<String, Double> ranksPrev = new HashMap<String, Double>();
        Map<String, Double> ranks = results.getRanks();

        double init = 1.0 / results.getGraph().size();
        double initial = (1 - dampingFactor) / results.getGraph().size();

        for (String urlKey : results.getGraph().keySet()) {
            ranksPrev.put(urlKey, init);
        }

        for (int i = 0; i < numberOfLoopsInRankComputing; i++) {
            for(String urlKey : results.getGraph().keySet()){    // ключи - url адреса страниц из urlContent

                double  summ = 0.0;
                for (String ref: results.getGraph().get(urlKey)){   //список url-адресов, которые ссылаются на ключ
                   summ += ranksPrev.get(ref)/totalNumberOfReferencesFromPage(ref,results.getGraph());
                }
                
                ranks.put(urlKey, (initial + dampingFactor * summ));
            }

            ranksPrev.clear();
            ranksPrev.putAll(ranks);

        }
        return results;
    }



    /**
     * Количество ссылок с текущей страницы на другие
     *
     * @param pageUrl страница для которой необходимо определить кол-во ссылок
     * @param graph   карта, ключами которой являются url адреса страниц из urlContent-а,
     *                а значениями - список url-адресов, которые ссылаются на ключ
     * @return количество ссылок с текущей страницы на другие
     */
    private int totalNumberOfReferencesFromPage(String pageUrl, Map<String, List<String>> graph) {
        int count = 0;
        if (null != graph) {
            for (String url : graph.keySet()) {
                if (!pageUrl.equals(url)) {

                    for (String refUrl : graph.get(url)) {
                        if (pageUrl.equals(refUrl)) {
                            count++;
                            //break; //можно если ссылка встречается один раз
                        } //if
                    } //for (String refUrl: graph.get(url))
                } //if
            } //for (String url: graph.keySet()
        }

        return count;
    }
}
