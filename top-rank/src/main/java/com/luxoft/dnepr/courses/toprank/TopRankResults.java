package com.luxoft.dnepr.courses.toprank;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 */
public class TopRankResults {
    private Map<String, List<String>> graph = new HashMap<String, List<String>>();
    private Map<String, List<String>> reverseGraph = new HashMap<String, List<String>>();
    private Map<String, List<String>> index = new HashMap<String, List<String>>();
    private Map<String, Double> ranks = new HashMap<String, Double>();

    public TopRankResults() {
    }

    public Map<String, List<String>> getGraph() {
        return graph;
    }

    public Map<String,List<String>> getReverseGraph() {
        return reverseGraph;
    }

    public Map<String, List<String>> getIndex() {
        return index;
    }

    public Map<String, Double> getRanks() {
        return ranks;
    }

    public void parsUrlContent(Map<String, String> urlContent) {
        if (null != urlContent) {
            createGraph(urlContent);
            createReverseGraph(urlContent);
            createIndex(urlContent);
        }
    }

    private void createReverseGraph(final Map<String, String> urlContent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                for (String page : urlContent.keySet()) {
                    if (null == reverseGraph.get(page)) {
                        reverseGraph.put(page, new ArrayList<String>());
                    }
                    List<String> ref = retrieveLinks(urlContent.get(page)); //список ссылок со стр
                    if (null != ref) {
                        reverseGraph.put(page, ref);
                    } else {
                        reverseGraph.put(page, new ArrayList<String>());
                    }
                }// for
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void createGraph(final Map<String, String> urlContent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (String page : urlContent.keySet()) {
                    if (null == graph.get(page)) {
                        graph.put(page, new ArrayList<String>());
                    }

                    List<String> ref = retrieveLinks(urlContent.get(page)); //список ссылок со стр
                    if (null != ref) {
                        for (int i = 0; i < ref.size(); i++) {
                            if (null == graph.get(ref.get(i))) {
                                List<String> list = new ArrayList<String>();
                                list.add(page);
                                graph.put(ref.get(i), list);
                            } else {
                                List<String> graphUrlValue = graph.get(ref.get(i)); // получили список ссылок
                                if (!isUrl(graphUrlValue, page)) {  //проверяем есть ли данный адрес в списке
                                    graphUrlValue.add(page);
                                    graph.put(ref.get(i), graphUrlValue);
                                }
                            }
                        }  // for
                    }  //if (null != ref)
                }  // for (String page : urlContent.keySet())
            }  //run()
        }; //new Runnable()
        Thread thread = new Thread(runnable);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Возвращает лист ссылок с данной страницы
     *
     * @param content содержимое страницы
     * @return лист ссылок с данной страницы
     */
    private List<String> retrieveLinks(String content) {
        ArrayList<String> linksList = new ArrayList<String>();
        Pattern pattern = Pattern.compile("<a\\s+href\\s*=\\s*\"?(.*?)[\"|>]", Pattern.CASE_INSENSITIVE);
//        List<String> graphUrl = Arrays.asList(content.split("<a href=([^>]+)>([^<]+)</a>"));
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
            String link = matcher.group(1).trim();
            if (link.length()<1){  // пустые ссылки
                continue;
            }
            if (link.charAt(0) == '#'){   // ссылки на текущуюю страницу
                continue;
            }
            if (link.indexOf("mailto:") != -1){    // почтовые ссылки
                continue;
            }
            if (link.toLowerCase().indexOf("javascript") != -1){   // ссылки на JavaScript
                continue;
            }
            linksList.add(link);
        }
        return linksList;
    }


    private void createIndex(final Map<String, String> urlContent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (String page : urlContent.keySet()) {
                    List<String> words = new ArrayList<String>();
                    words.addAll(Arrays.asList((urlContent.get(page)).split("[\t\n\\s]")));
                    //разбили содержимое страницы на слова
                    for (int i = 0; i < words.size(); i++) {
                        //проверяем есть ли слово в index
                        if (null == index.get(words.get(i))) {
                            List<String> list = new ArrayList<String>();
                            list.add(page);
                            index.put(words.get(i), list); // нет - добовляем
                        } else {
                            List<String> url = index.get(words.get(i)); // получили адреса страниц содержащих "слово"
                            url.add(page);
                            index.put(words.get(i), url);
                        }
                    }  // for (int i=0; i<words.size(); i++)
                }//for
            }//run()
        };
        Thread thread = new Thread(runnable);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Есть ли списке элемент с таким адресом
     *
     * @param list       список адресов
     * @param currentUrl искомый адрес
     * @return true - есть, false - нет
     */
    private boolean isUrl(List<String> list, String currentUrl) {
        for (String url : list) {
            if (currentUrl.equals(url))
                return true;
        }
        return false;
    }



    @Override
    public String toString() {
        return "TopRankResults{" +
                "\n\ngraph =" + graph.toString() +
                "\n\nreverseGraph =" + reverseGraph.toString() +
                ", \n\nindex =" + index.toString() +
                ", \n\nranks = page: " + ranks.size() + " " + ranks.toString() +
                '}';
    }

}
