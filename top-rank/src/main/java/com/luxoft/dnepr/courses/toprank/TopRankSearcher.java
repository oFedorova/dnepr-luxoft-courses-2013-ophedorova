package com.luxoft.dnepr.courses.toprank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: Phedorova
 * Date: 06.06.13
 * Time: 2:14
 */
public class TopRankSearcher {

    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;

    public TopRankSearcher() {
    }

    public synchronized TopRankResults execute(List<String> urls) {
        TopRankExecutor topRankExecutor =
                new TopRankExecutor(DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);

        return topRankExecutor.execute((new TopRankSearcher()).createUrlContent(urls));
    }

    public synchronized TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {
        TopRankExecutor topRankExecutor =
                new TopRankExecutor(dampingFactor, numberOfLoopsInRankComputing);

        return topRankExecutor.execute((new TopRankSearcher()).createUrlContent(urls));
    }


    /**
     * Загружает Web-страницу с данного адреса и возвращает содержимое как одну большую строку
     *
     * @param stringPageUrl - строковое представление URL
     * @return возвращает содержимое Web-страницы как одну большую строку
     */
    private String downloadPage(final String stringPageUrl) throws IOException {
        URL pageUrl = new URL(stringPageUrl);  // throws MalformedURLException
        // Открыть соединение для чтения
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(
                        pageUrl.openStream()));

        // Прочитать страницу в буфер
        String line;
        StringBuffer pageBuffer = new StringBuffer();
        while ((line = reader.readLine()) != null) {
            pageBuffer.append(line);
        }

        return pageBuffer.toString();
    }






    /**
     * Создание карты <адрес, содержимое>
     *
     * @param urls список адресов
     * @return карта <адрес, содержимое>
     */
    private Map<String, String> createUrlContent(List<String> urls) {
        Map<String, String> urlContent = new ConcurrentHashMap<String, String>();
        ExecutorService exception = Executors.newCachedThreadPool();

        List<Callable<ArrayList<String>>> callables = new ArrayList<Callable<ArrayList<String>>>();

        for (final String page : urls) {

            Callable<ArrayList<String>> callable = new Callable<ArrayList<String>> () {
                @Override
                public ArrayList<String> call()throws IOException{

                    ArrayList<String> urlContent1 = new ArrayList<String>(2);
                    urlContent1.add(page);
                    urlContent1.add(downloadPage(page));

                    return urlContent1;
                }
            };
            callables.add(callable);
        }

        try {
            List <Future <ArrayList<String>>> futures = exception.invokeAll(callables);
            for (int i=0; i<futures.size(); i++){
                try {

                    urlContent.put(futures.get(i).get().get(0), futures.get(i).get().get(1));

                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        exception.shutdown();

        return urlContent;
    }
}